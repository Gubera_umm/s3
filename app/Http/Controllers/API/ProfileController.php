<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Log;
use Response;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    /**
     * Update Profile Picture.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePic(Request $request, $id)
    {
        try {
                // File Upload
                $image = $request->image;
                $filePath = 'accounts/profile/' . time() . '2' . '.' . $image->getClientOriginalExtension();

                $s3 = \Storage::disk('s3');
                $uploaded = $s3->put($filePath, file_get_contents($image));

                if ($uploaded) {
                    // Unlink existing image from bucket
                    $response = response()->json(['message' => 'Success'], 200);
                } else {
                    $response = response()->json(['message' => 'error'], 401);
                }

            return $response;
        } catch (\Exception $e) {
            Log::critical($e);
            return $response = response()->json(['message' => $e], 400);
        }
    }
}
